-- Create a list containing all integers within a given range.

range :: Int -> Int -> [Int]
range n m = [ x | x <- [n..m] ]
