-- Find the last but one element of a list.

myButLast :: [a] -> a
myButLast [] = error "Oops!"
myButLast [x] = error "Oops!"
myButLast [x, y] = x
myButLast (x:xs) = myButLast xs
