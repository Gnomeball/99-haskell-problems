-- Modify the result of problem 10 in such a way that if an element has no duplicates it is simply copied into the result list.
-- Only elements with duplicates are transferred as (N E) lists.

import Data.List

encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = [ (length x, head x) | x <- group xs ]

-- From 10 ^
-- From 11 V

data ListItem a = Single a | Multiple Int a
    deriving (Show)

encodeModified :: (Eq a) => [a] -> [ListItem a]
encodeModified = map encodeMod . encode
    where
        encodeMod (1, char) = Single char
        encodeMod (n, char) = Multiple n char


-- encode xs = [ ((if length x == 1 then "Single" else "Multiple"), head x) | x <- group xs ]