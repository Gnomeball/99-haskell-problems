-- Find the number of elements of a list.

myLength :: [a] -> Int
myLength (xs) = sum [ 1 | x <- xs ]
