-- Extract a given number of randomly selected elements from a list.

import System.Random

rnd_select :: [a] -> Int -> [a]
rnd_select [] _ = []
rnd_select xs n = if n > 0
    then xs -- YeaNope
    else error "Oops!"
