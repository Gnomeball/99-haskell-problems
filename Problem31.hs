-- Determine whether a given integer number is prime.

isPrime :: Int -> Bool
isPrime n = length factors == 2 where factors = [ x | x <- [1..n], mod n x == 0 ]
