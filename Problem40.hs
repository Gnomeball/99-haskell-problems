-- Goldbach's conjecture.

goldbach :: Int -> (Int, Int)
goldbach n = head [ (x, y) | x <- primesToN, y <- primesToN, x + y == n]
    where
        primesToN = primesR 2 (n-2)

-- From Problem 39

primesR :: Int -> Int -> [Int]
primesR n m = [ x | x <- [n..m], length (factors x) == 2 ]
    where factors n = [ x | x <- [1..n], mod n x == 0]

-- As a Single Function

goldbach' :: Int -> (Int, Int)
goldbach' n = head [ (x, y) | x <- primesToN, y <- primesToN, x + y == n]
    where
        primesToN = [ x | x <- [2..(n-2)], length (factors x) == 2 ]
        factors n = [ x | x <- [1..n], mod n x == 0]