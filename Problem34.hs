-- Calculate Euler's totient function phi(m).
-- phi(m) is defined as the number of positive integers r (1 <= r < m) that are coprime to m.
-- Example: m = 10: r = 1,3,7,9; thus phi(m) = 4.

totient :: Int -> [Int]
totient n = [ x | x <- [1..n], coPrime x n == True ]
    where coPrime n m = last [ x | x <- [1..max n m], mod n x == 0, mod m x == 0] == 1

{-

totient :: Int -> [Int]
totient n = [ x | x <- [1..n], coPrime x n == True ]
    where coPrime n m = last factors == 1
    where factors = [ x | x <- [1..max n m], mod n x == 0, mod m x == 0]

-}
