-- Split a list into two parts; the length of the first part is given.

split :: [a] -> Int -> [[a]]
split xs n = [(take n xs), (drop n xs)]