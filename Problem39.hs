-- A list of prime numbers.

primesR :: Int -> Int -> [Int]
primesR n m = [ x | x <- [n..m], length (factors x) == 2 ]
    where factors n = [ x | x <- [1..n], mod n x == 0]
