-- Find the K'th element of a list. The first element in the list is number 1.

elementAt :: [a] -> Int -> a
elementAt [] _ = error "Oops!"
elementAt (x:xs) 0 = error "Oops!"
elementAt (x:xs) 1 = x
elementAt (x:xs) n = elementAt xs (n-1)
