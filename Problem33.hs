-- Determine whether two positive integer numbers are coprime.
-- Two numbers are coprime if their greatest common divisor equals 1.

coPrime :: Int -> Int -> Bool
coPrime n m = last factors == 1
    where factors = [ x | x <- [1..max n m], mod n x == 0, mod m x == 0]
