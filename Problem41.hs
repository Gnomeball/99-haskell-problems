-- Given a range of integers by its lower and upper limit, print a list of all even numbers and their Goldbach composition.

goldbachList :: Int -> Int -> [(Int, Int)]
goldbachList n m = map goldbach $ filter even [n..m]

-- From Problem 40

goldbach :: Int -> (Int, Int)
goldbach n = head [ (x, y) | x <- primesToN, y <- primesToN, x + y == n]
    where
        primesToN = [ x | x <- [2..(n-2)], length (factors x) == 2 ]
        factors n = [ x | x <- [1..n], mod n x == 0]
