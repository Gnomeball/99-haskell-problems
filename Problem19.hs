-- Rotate a list N places to the left.

rotate :: [a] -> Int -> [a]
rotate xs n = if n >= 0
    then
        drop n xs ++ take n xs
    else
        let m = ((length xs) + n) in drop m xs ++ take m xs
