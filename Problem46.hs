-- Define predicates and/2, or/2, nand/2, nor/2, xor/2, impl/2 and equ/2 (for logical equivalence) which succeed or fail according to the result of their respective operations;
-- e.g. and(A,B) will succeed, if and only if both A and B succeed.
-- A logical expression in two variables can then be written as in the following example: and(or(A,B),nand(A,B)).
-- Now, write a predicate table/3 which prints the truth table of a given logical expression in two variables.

logicalAnd :: Bool -> Bool -> Bool
logicalAnd a b = a && b

logicalOr :: Bool -> Bool -> Bool
logicalOr a b = a || b

logicalNand :: Bool -> Bool -> Bool
logicalNand a b = not (logicalAnd a b)

logicalNor :: Bool -> Bool -> Bool
logicalNor a b = not (logicalOr a b)

logicalXor :: Bool -> Bool -> Bool
logicalXor a b = not (logicalEqu a b)

logicalImpl :: Bool -> Bool -> Bool
logicalImpl a b = logicalOr (not a) b

logicalEqu :: Bool -> Bool -> Bool
logicalEqu a b = a == b

table :: (Bool -> Bool -> Bool) -> IO()
table f = putStrLn $ concatMap (++ "\n" )
    [ show a ++ " " ++ show b ++ " " ++ show (f a b) | a <- [True, False], b <- [True, False] ]

-- Aparently this also works for Problem 47?
