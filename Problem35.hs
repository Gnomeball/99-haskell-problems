-- Determine the prime factors of a given positive integer.
-- Construct a flat list containing the prime factors in ascending order.

primeFactors :: Int -> [Int]
primeFactors n = factors n 2
    where
        factors 1 _ = []
        factors n f
            | mod n f == 0 = f : factors (div n f) f
            | otherwise    = factors n (f + 1)
