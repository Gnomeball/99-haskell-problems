-- Determine the greatest common divisor of two positive integer numbers.

myGCD :: Int -> Int -> Int
myGCD n m = last [ x | x <- [1..max n m], mod n x == 0, mod m x == 0]
