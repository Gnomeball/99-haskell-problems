-- Run-length encoding of a list (direct solution).

import Data.List

data ListItem a = Single a | Multiple Int a
    deriving (Show)

encodeDirect :: (Eq a) => [a] -> [ListItem a]
encodeDirect [] = []
encodeDirect (x:xs) = encodeDirectMod 1 x xs
    
encodeDirectMod :: (Eq a) => Int -> a -> [a] -> [ListItem a]
encodeDirectMod n x [] = [encodeMod (n, x)]
encodeDirectMod n x xs = if x == (head xs)
    then encodeDirectMod (n + 1) x (tail xs)
    else [encodeMod (n, x)] ++ (encodeDirect xs)
    
encodeMod :: (Int, a) -> ListItem a
encodeMod (1, char) = Single char
encodeMod (n, char) = Multiple n char

-- Don't fully understand this 