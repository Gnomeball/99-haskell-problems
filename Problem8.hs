-- Eliminate consecutive duplicates of list elements.
-- The order of the elements should not be changed.

import Data.List

compress :: (Eq a) => [a] -> [a]
compress = map head . group