-- Decode a run-length encoded list.

data ListItem a = Single a | Multiple Int a
    deriving (Show)

decodeModified :: (Eq a) => [ListItem a] -> [a]
decodeModified = concatMap decodeMod
    where
        decodeMod (Single char) = [char]
        decodeMod (Multiple n char) = replicate n char